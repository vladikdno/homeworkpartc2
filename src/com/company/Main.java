package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //2. Ввести n строк с консоли. Вывести на консоль те строки, длина которых меньше средней.

        Scanner newSc = new Scanner(System.in); //Вызываем масив с названием newSc

        System.out.println("Enter number of rows: ");
        int numRows = Integer.parseInt(newSc.nextLine());   //Считали количество строк с консоли, которые ввел user с помощью nextLine()
        String[] rowsArray = new String[numRows];   //Определились с колиством индексов в массиве, с помощью int numRows
        int average = 0;        //переменная для вычесления средней длины
        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Enter the row number %d: ", i + 1)); //консоль говорит нам введи что-то начиная с первой строки
            String row = newSc.nextLine();   //сканер считывает то что ты вбил в консоли
            rowsArray[i] = row;     //теперь массиву rowsArray присваиваеться то, что мы написали выше
            average += rowsArray[i].length();   //для вычесления средней длины мы складываем 1 из массивов
        }
        average = average / numRows;    //средняя длина 1 из массивов

        System.out.println(String.format("Average (%d)\n", average));
        for (int i = 0; i < numRows; i++) {
            if (rowsArray[i].length() < average)    //если один из массивов меньше средней длины, то выводим его экран
                System.out.println("(" + rowsArray[i].length() + "): \"" + rowsArray[i] + "\"");
        }
    }
}
